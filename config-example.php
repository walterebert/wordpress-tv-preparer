<?php
/**
 * Example configuration file. All settings are optional.
 */

return [
	'ffmpeg'           => '/usr/local/bin/ffmpeg', // Path to FFmpeg binary.
	'ffprobe'          => '/usr/local/bin/ffprobe', // Path to FFprobe binary.
	'aac'              => 'libfdk_aac', // AAC encoder to use (default: aac -strict experimental). See available AAC encoders: ffmpeg -codecs | grep aac.
	'sources'          => __DIR__ . '/videos/in', // Source directory.
	'source_extension' => 'mp4', // Source video file extension (default: MTS).
	'target'           => __DIR__ . '/videos/out', // Target directory.
	'screen'           => '1920x1080', // Video screen size in pixels (default: same as source video).
	'overwrite'        => false, // Overwrite already rendered videos.
	'crf'              => 18, // Constant Rate Factor for video quality. 0 = lossless, 20 = default, 51 = worst.
	'framerate'        => 50, // Video frame rate (default: 25).
	'audio_quality'    => '-ac 2 -b:a 96k -ar 44100', // Audio quality (default: -ac 2 -b:a 128k -ar 48000).
	'tmp'              => __DIR__ . '/tmp', // Directory for temporary files.
	'intro' => [ // Intro sequences.
		[
			'duration' => 1, // Duration in seconds.
			'src' => __DIR__ . '/images/wcfra_slide_opening.png', // Intro image.
		],
		[
			'duration' => 2, // Duration in seconds.
			'src' => __DIR__ . '/images/wcfra_slide.png', // Background image.
			'title' => [ // Session title.
				'x' => 400, // Horizontal position in pixels. Ommit for centered text.
				'y' => 400, // Vertical position in pixels.
				'fontfile' => __DIR__ . '/fonts/RubikOne-Regular.ttf',
				'fontsize' => 64, // Font size in pixels.
				'fontcolor' => 'black' // Font color https://ffmpeg.org/ffmpeg-utils.html#Color
			],
			'speaker' => [ // Speaker name.
				'y' => 650,
				'fontfile' => __DIR__ . '/fonts/RobotoMono-Bold.ttf',
				'fontsize' => 36,
				'fontcolor' => 'black',
			],
		],
	],
];
