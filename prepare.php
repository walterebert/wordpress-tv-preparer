<?php
/**
 * Prepare videos for upload on WordPress.tv
 */

require __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'init.php';
require __DIR__ . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR . 'functions.php';

// Video settings.
$extension = 'MTS';
if ( ! empty( $config['source_extension'] ) && is_string( $config['source_extension'] ) ) {
	$extension = $config['source_extension'];
}
$screen = '';
if ( ! empty( $config['screen'] ) && is_string( $config['screen'] ) ) {
	$screen = ' -s ' . escapeshellarg( $config['screen'] ) . ' ';
}

// Audio settings.
$audio_quality = '-ac 2 -b:a 128k -ar 48000';
if ( ! empty( $config['audio_quality'] ) && is_string( $config['audio_quality'] ) ) {
	$audio_quality = $config['audio_quality'];
}

// Track temporary files.
$tmp = [];

// Get video meta data files.
$files = [];
if ( is_file( $config['sources'] ) ) {
	$files[] = $config['sources'];
} else {
	$files = glob( $config['sources'] . DIRECTORY_SEPARATOR . '*.' . $extension);
}

// Check if any video were found.
if ( empty( $files ) ) {
	die( 'Could not find any source videos in:' . PHP_EOL . escapeshellcmd( $config['sources'] ) . PHP_EOL);
}

foreach ( $files as $source ) {
	$filename = basename( $source );
	$file_base = preg_replace( '/\.' . $extension . '$/', '', $filename );
	$source_base = preg_replace( '/\.' . $extension . '$/', '', $source );

	if ( preg_match( '/--.*\.' . $extension . '$/', $source ) ) {
		// Skipping video.
		echo 'Skipping "' . $filename . '"' . PHP_EOL;
	} elseif ( $source ) {
		// Create new video.
		$destination = $config['target'] . DIRECTORY_SEPARATOR . $file_base . '.mp4';
		if ( $config['target'] === $config['sources'] ) {
			$destination = $config['target'] . DIRECTORY_SEPARATOR . $file_base . '-final.mp4'; 
		}

		if ( empty( $config['overwrite'] ) && file_exists( $destination ) ) {
			// Do not overwrite already rendered video.
			echo 'Video "' . escapeshellcmd( $destination ) . '" already exists!' . PHP_EOL;
		} else {
			// Set Constant Rate Factor.
			$crf = 20;
			if ( isset( $config['crf'] ) && is_numeric( $config['crf'] ) ) {
				$crf = (int) $config['crf'];
			}

			// Set frame rate.
			$framerate = 25;
			if ( ! empty( $config['framerate'] ) ) {
				// Use frame rate from configuration.
				$framerate = $config['framerate'];
			} else {
				// Autodetect frame rate.
				$command = escapeshellcmd( $config['ffprobe'] ) . ' -show_streams ' . escapeshellarg( $source );
				echo $command . PHP_EOL;
				$ffprobe = shell_exec( $command );
				$count = preg_match( '%r_frame_rate=([0-9]+/[0-9]+)%um', $ffprobe, $matches );

				if ( isset( $matches[1] ) ) {
					$framerate = $matches[1];
				}
			}

			// Get screen size if not set.
			if ( empty( $screen ) ) {
				$width = 0;
				$count = preg_match( '%width=([0-9]+)%um', $ffprobe, $matches );
				if ( isset( $matches[1] ) ) {
					$width = $matches[1];
				}

				$height = 0;
				$count = preg_match( '%height=([0-9]+)%um', $ffprobe, $matches );
				if ( isset( $matches[1] ) ) {
					$height = $matches[1];
				}

				if ( $width && $height ) {
					$screen = ' -s ' . $width .  'x' . $height . ' ';  
				}
			}

			$filter = '';

			// Intro sequences. 
			$intro = '';
			$intro_count = 0;
			if ( $config['intro'] ) {
				$intro_count = count( $config['intro'] );
			}
			if ( $intro_count ) {
				$i = 0;

				// Render intro sequences.
				foreach( $config['intro'] as $sequence ) {
					// Check if source is an image.
					$loop = '';
					if ( preg_match( '/\.(png|jpg)$/iu', $sequence['src'] ) ) {
						$loop = ' -loop 1 ';
					}

					$duration = 1;
					if( $sequence['duration'] ) {
						$duration = (int) $sequence['duration'];
					}

					// Add text to intro sequence.
					$sequence_filter = '';
					if ( ! empty( $sequence['title'] ) ) {
						// Add session title.
						$textfile = $source_base . '--title.txt';
						if ( file_exists( $textfile ) ) {
							$sequence_filter .= ' -filter_complex ' . wptv_prep_drawtext( array_merge( $sequence['title'], ['textfile' => $textfile ] ) );
						}
					}
					if ( ! empty( $sequence['speaker'] ) ) {
						// Add session speaker.
						$textfile = $source_base . '--speaker.txt';
						if ( file_exists( $textfile ) ) {
							if ( $sequence_filter ) {
								$sequence_filter .= ',';
							} else {
								$sequence_filter .= ' -filter_complex ';
							} 
							$sequence_filter .= wptv_prep_drawtext( array_merge( $sequence['speaker'], ['textfile' => $textfile ] ) );
						}
					}
					
					// Check for predefined intro image.
					$intro_suffix = '--intro-' . ( $i + 1 );
					if ( file_exists( $source_base . $intro_suffix .'.png' ) ) {
						$sequence_filter = '';
						$loop = ' -loop 1 ';
						$sequence['src'] = $source_base . $intro_suffix .'.png';
					} elseif ( file_exists( $source_base . $intro_suffix . '.jpg' ) ) {
						$sequence_filter = '';
						$loop = ' -loop 1 ';
						$sequence['src'] = $source_base . $intro_suffix . '.jpg';
					}

					$tmpfile = $tmp[] = $config['tmp'] . DIRECTORY_SEPARATOR . $file_base . '-intro-' . ( $i + 1 ) . '.mp4';

					$command = escapeshellcmd( $config['ffmpeg'] ) .
				    	' -y ' .
						$loop .
						' -i ' . escapeshellarg( $sequence['src'] ) .
						' -f lavfi -i anullsrc ' . // Add silence for concatenation.
						$sequence_filter .
				  		' -c:a ' . escapeshellcmd( $config['aac'] ) .
				  		' -c:v libx264 -pix_fmt yuv420p ' .
						' ' . escapeshellcmd( $audio_quality ) . ' ' .
				  		' -r ' . $framerate . ' ' .
						' -crf 0 ' .
				  		$screen . ' ' .
						' -t ' . $duration . ' ' .
				  		escapeshellarg( $tmpfile );

					echo $command . PHP_EOL;

					exec( $command );

					$intro .= ' -i ' . escapeshellarg( $tmpfile ) . ' ';

					$i++;
				}

				// Set arguments to concatenate video with FFmpeg. Reference: https://trac.ffmpeg.org/wiki/Concatenate.
				$graph = '';
				for( $i = 0; $i <= $intro_count ; $i++ ) {
					$graph .= '[' . $i . ':0] [' . $i . ':1] ';
				}
				$filter .= ' -filter_complex "' . $graph . ' concat=n=' . ( $intro_count + 1 ) . ':v=1:a=1 [v] [a]"';
				$filter .= ' -map "[v]" -map "[a]"';
			}

			// Render full video.
			$command = escapeshellcmd( $config['ffmpeg'] ) .
				' -y ' .
				$intro .
				' -i ' . escapeshellarg( $source ) .
				$filter .
				' -c:a ' . escapeshellcmd( $config['aac'] ) .
				' ' . escapeshellcmd( $audio_quality ) . ' ' .
				' -c:v libx264 -pix_fmt yuv420p -profile:v high -level 3.2 -movflags faststart ' .
				' -r ' . $framerate . ' ' .
				' -crf ' . $crf . ' ' .
				$screen . ' ' .
				escapeshellarg( $destination );
				echo $command . PHP_EOL;
				exec( $command );
		}
	} else {
		// No source video.
		echo 'Could not find "' . $filename . '"' . PHP_EOL;
	}
}

// Delete temporary files.
if ( $tmp ) {
	foreach( $tmp as $file ) {
		unlink( $file );
	}
}		

echo PHP_EOL . 'Finished rendering videos.' . PHP_EOL . PHP_EOL;
echo 'The videos are in:' . PHP_EOL;
echo escapeshellcmd( $config['target'] ) . DIRECTORY_SEPARATOR . PHP_EOL . PHP_EOL;
echo 'You can upload them to WordPress.tv on http://wordpress.tv/submit-video/' . PHP_EOL . PHP_EOL ;
echo 'To upload to Amazon S3, request access on the WordPress Slack (#wordpresstv).' . PHP_EOL . PHP_EOL;
