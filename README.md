WordPress.TV Preparer
=====================

Script to prepare videos for upload on [WordPress.tv](http://wordpress.tv/).
Final videos are in MP4 format optimised for use on the web. Source videos can
be in any format that FFmpeg supports (usually all common formats).

Requirements
------------
- PHP 5.4 or later
- [FFmpeg](https://ffmpeg.org/)

Usage
-----
```
php prepare.php <source directory or file> <target directory>
```

You can define directories by creating a config.php file. See the
config-example.php file for possible settings. After this you can leave out
the directory arguments.
```
php prepare.php
```

You can add intro sequences from images (PNG/JPG). It is also possible to add
a session title and speaker name to the intro. To add text to a video you have
to create text files for the session title and the speaker name. The text file
must be placed in the same directory as the source video file. Text must be
UTF-8 encoded and can have multiple lines (emoji does not seem to work). The
text file name must use the following convention (notice the double dash):
- \<video file name without file extension\>--title.txt
- \<video file name without file extension\>--speaker.txt

The prepare script will look for them if title or speaker argument are given
for a intro sequence in config.php.

Example:
```
'intro' => [ // Intro sequences.
	[
		'duration' => 2, // Duration in seconds.
		'src'      => realpath( 'images/wcfra_slide.png' ), // Background image.
		'title' => [ // Session title.
			'x' => 400, // Horizontal position in pixels.
			'y' => 400, // Vertical position in pixels.
			'fontfile' => realpath( 'fonts/RubikOne-Regular.ttf' ),
			'fontsize' => 64, // Font size in pixels.
			'fontcolor' => 'black'
		],
		'speaker' => [ // Speaker name.
			'x' => 400,
			'y' => 650,
			'fontfile' => realpath( 'fonts/RobotoMono-Bold.ttf' ),
			'fontsize' => 36,
			'fontcolor' => 'black',
		],
	],
],
```
You can use a predefined intro image for a video in case text cannot be added
correctly by the script (for example adding emojis). To skip adding text to use
the predefined image, create an image using the following convention:
- \<video file name without file extension\>--intro-\<intro sequence number\>.png

Example:
```
/path/to/event-name-speaker-name-session-title.MTS // Source video file.
/path/to/event-name-speaker-name-session-title--title.txt // Session title text.
/path/to/event-name-speaker-name-session-title--speaker.txt // Speaker name text.
/path/to/event-name-another-speaker-name-session-title.MTS // Source video file.
/path/to/event-name-another-speaker-name-session-title--intro-2.png // Predefined image for intro sequence 2. 
```

TODO
----
- Update documentation.

License
-------
[MIT](https://opensource.org/licenses/MIT)

Author
------
[Walter Ebert](http://walterebert.com/)

References
----------
- https://ffmpeg.org/
- https://ffmpeg.org/ffmpeg.html
- https://trac.ffmpeg.org/wiki/Encode/H.264
- https://trac.ffmpeg.org/wiki/Concatenate#filter
- https://ffmpeg.org/ffmpeg-utils.html#Color
- http://slhck.info/articles/crf
- http://slhck.info/video-encoding
