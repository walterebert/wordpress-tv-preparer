<?php
/**
 * Initialize PHP scripts
 */

// Check if CLI arguments are available.
if ( empty( $argv ) ) {
	die( "Script cannot take arguments from the command line. Please enable \"register_argc_argv\" in php.ini.\n" );
}

// Load config file.
$config = [];
$config_file = __DIR__ . DIRECTORY_SEPARATOR . '..' .  DIRECTORY_SEPARATOR . 'config.php';
if ( file_exists( $config_file ) ) {
	$config = require $config_file;    
}

// Set program paths.
if ( empty( $config['ffmpeg'] ) ) {
	$config['ffmpeg'] = 'ffmpeg';
}
if ( empty( $config['ffprobe'] ) ) {
	$config['ffprobe'] = 'ffprobe';
}
if ( empty( $config['aac'] ) ) {
	$config['aac'] = 'aac -strict experimental';
}


// Set source and target directories if CLI arguments are given.
if ( ! empty( $argv[1] ) && is_dir( $argv[1] ) ) {
	$config['sources'] = $config['target'] = $argv[1];
	if ( ! empty( $argv[2] ) && is_dir( $argv[2] ) ) {
		$config['target'] = $argv[2];
	}
}

// Ensure source, target and temporary directories are set
if ( empty( $config['sources'] ) ) {
	$config['sources'] = realpath( __DIR__ . '/..' );
}
if ( empty( $config['target'] ) ) {
	$config['target'] = $config['sources'];
}
if ( empty( $config['tmp'] ) ) {
	$config['tmp'] = realpath( __DIR__ . '/../tmp' );
}

// Remove trailering slashes.
$config['sources'] = preg_replace( '%[/\\\]$%', '', trim( $config['sources'] ) );
$config['target'] = preg_replace( '%[/\\\]$%', '', trim( $config['target'] ) );
$config['tmp'] = preg_replace( '%[/\\\]$%', '', trim( $config['tmp'] ) );

// Ensure directories exists.
if ( ! file_exists( $config['sources'] ) ) {
	die( 'Source directory "' . escapeshellcmd( $config['sources'] ) . '" does not exist.' . PHP_EOL );
}
if ( ! file_exists( $config['target'] ) ) {
	mkdir( $config['target'], 0755, true );
}
if ( ! file_exists( $config['tmp'] ) ) {
	mkdir( $config['tmp'], 0755, true );
}
