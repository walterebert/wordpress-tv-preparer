<?php
/**
 * Functions
 */

/**
 * Construct FFmpeg drawtext filter.
 * Reference: https://ffmpeg.org/ffmpeg-filters.html#drawtext-1
 * @param array Drawtext parameters
 * @return string
 */
function wptv_prep_drawtext( array $params ) {
	if ( empty( $params['textfile'] ) ) {
		return '';
	}

	$x = '(w-text_w)/2'; // Center horizontally.
	if ( ! empty( $params['x'] ) ) {
		$x = $params['x'];
	}

	$y = '(h-text_h)/2'; // Center vertically.
	if ( ! empty( $params['y'] ) ) {
		$y = $params['y'];
	}

	$fontsize = 'fontsize=40:';
	if ( ! empty( $params['fontsize'] ) and is_numeric( $params['fontsize'] ) ) {
		$fontsize = 'fontsize=' . $params['fontsize'] . ':';
	}
	$fontfile = 'fontfile=' . realpath( __DIR__ . '/../fonts' ) . DIRECTORY_SEPARATOR . 'SourceSansPro-Regular.ttf:';
	if ( ! empty( $params['fontfile'] ) and file_exists( $params['fontfile'] ) ) {
		$fontfile = 'fontfile=' . escapeshellarg( $params['fontfile'] ) . ':';
	}

	return 'drawtext="fix_bounds=true:' . $fontsize . $fontfile . 'textfile=' . escapeshellarg( $params['textfile'] ) . ':x=' . $x . ':y=' . $y . '"';
}
